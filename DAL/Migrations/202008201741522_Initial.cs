﻿namespace DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Cart",
                c => new
                    {
                        cartId = c.Int(nullable: false, identity: true),
                        productId = c.Int(nullable: false),
                        customerId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.cartId)
                .ForeignKey("dbo.Customers", t => t.customerId, cascadeDelete: true)
                .ForeignKey("dbo.Products", t => t.productId, cascadeDelete: true)
                .Index(t => t.productId)
                .Index(t => t.customerId);
            
            CreateTable(
                "dbo.Customers",
                c => new
                    {
                        customerId = c.Int(nullable: false, identity: true),
                        firstName = c.String(),
                        lastName = c.String(),
                        email = c.String(),
                        password = c.String(),
                        roleId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.customerId)
                .ForeignKey("dbo.RoleList", t => t.roleId, cascadeDelete: true)
                .Index(t => t.roleId);
            
            CreateTable(
                "dbo.RoleList",
                c => new
                    {
                        roleId = c.Int(nullable: false, identity: true),
                        roleName = c.String(),
                    })
                .PrimaryKey(t => t.roleId);
            
            CreateTable(
                "dbo.Products",
                c => new
                    {
                        productId = c.Int(nullable: false, identity: true),
                        productName = c.String(),
                        price = c.Single(nullable: false),
                        quantity = c.Int(nullable: false),
                        productImage = c.String(),
                        categoryId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.productId)
                .ForeignKey("dbo.Categories", t => t.categoryId, cascadeDelete: true)
                .Index(t => t.categoryId);
            
            CreateTable(
                "dbo.Categories",
                c => new
                    {
                        categoryId = c.Int(nullable: false, identity: true),
                        categoryName = c.String(),
                    })
                .PrimaryKey(t => t.categoryId);
            
            CreateTable(
                "dbo.Orders",
                c => new
                    {
                        orderId = c.Int(nullable: false, identity: true),
                        date = c.DateTime(nullable: false),
                        total_price = c.Long(nullable: false),
                        custId = c.Int(nullable: false),
                        productId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.orderId)
                .ForeignKey("dbo.Customers", t => t.custId, cascadeDelete: true)
                .ForeignKey("dbo.Products", t => t.productId, cascadeDelete: true)
                .Index(t => t.custId)
                .Index(t => t.productId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Orders", "productId", "dbo.Products");
            DropForeignKey("dbo.Orders", "custId", "dbo.Customers");
            DropForeignKey("dbo.Cart", "productId", "dbo.Products");
            DropForeignKey("dbo.Products", "categoryId", "dbo.Categories");
            DropForeignKey("dbo.Cart", "customerId", "dbo.Customers");
            DropForeignKey("dbo.Customers", "roleId", "dbo.RoleList");
            DropIndex("dbo.Orders", new[] { "productId" });
            DropIndex("dbo.Orders", new[] { "custId" });
            DropIndex("dbo.Products", new[] { "categoryId" });
            DropIndex("dbo.Customers", new[] { "roleId" });
            DropIndex("dbo.Cart", new[] { "customerId" });
            DropIndex("dbo.Cart", new[] { "productId" });
            DropTable("dbo.Orders");
            DropTable("dbo.Categories");
            DropTable("dbo.Products");
            DropTable("dbo.RoleList");
            DropTable("dbo.Customers");
            DropTable("dbo.Cart");
        }
    }
}
