﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Models
{
    [Table("Cart")]
    public class Cart
    {
        [Key]
        [Column("cartId")]
        public int cartId { get; set; }

        [ForeignKey("Products")]
        [Column("productId")]
        public int productId { get; set; }
       

        [ForeignKey("Customers")]
        [Column("customerId")]
        public int customerId { get; set; }

        public virtual Customers Customers { get; set; }
        public virtual Products Products { get; set; }
    }
}
