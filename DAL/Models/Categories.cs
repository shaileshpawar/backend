﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Models
{
    [Table("Categories")]
    public class Categories
    {
        [Key]
        [Column("categoryId")]
        public int categoryId { get; set; }
        [Column("categoryName")]
        public string categoryName { get; set; }
        public virtual ICollection<Products> Products { get; set; }
    }
}
