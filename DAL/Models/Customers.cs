﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Models
{
    [Table("Customers")]
    public class Customers
    {
        [Key]
        [Column("customerId")]
        public int customerId { get; set; }
        [Column("firstName")]
        public string firstName { get; set; }
        [Column("lastName")]
        public string lastName { get; set; }
        [Column("email")]
        public string email { get; set; }

        [Column("password")]
        public string password { get; set; }

        [ForeignKey("RoleList")]
        [Column("roleId")]
        public int roleId { get; set; }

        public virtual RoleList RoleList { get; set; }

       
    }
}
