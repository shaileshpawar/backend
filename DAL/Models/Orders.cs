﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Models
{
    [Table("Orders")]
    public class Orders
    {
        [Key]
        [Column("orderId")]
        public int orderId { get; set; }
        [Column("date")]
        public DateTime date { get; set; }
        [Column("total_price")]
        public long totalPrice { get; set; }

        [ForeignKey("Customers")]
        [Column("custId")]
        public int customerId { get; set; }
       
        [ForeignKey("Products")]
        [Column("productId")]
        public int productId { get; set; }
        public virtual Products Products { get; set; }
        public virtual Customers Customers { get; set; }

    }
}
