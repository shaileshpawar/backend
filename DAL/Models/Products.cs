﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Models
{
    [Table("Products")]
    public class Products
    {
        [Key]
        [Column("productId")]
        public int productId { get; set; }

        [Column("productName")]
        public string productName { get; set; }
        [Column("price")]
        public double price { get; set; }
        [Column("quantity")]
        public int quantity { get; set; }

        [Column("productImage")]
        public string productImage { get; set; }

        [ForeignKey("Categories")]
        [Column("categoryId")]
        public int categoryId { get; set; }

        [Column("productStatus")]
        public string productStatus { get; set; }

        public virtual Categories Categories { get; set; }

    }
}
