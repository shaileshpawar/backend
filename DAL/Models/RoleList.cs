﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Models
{
    [Table("RoleList")]
    public class RoleList
    {
        [Key]
        [Column("roleId")]
        public int roleId { get; set; }
        [Column("roleName")]
        public string roleName { get; set; }
        public virtual ICollection<Customers> CustomerRoleList { get; set; }
    }
}
