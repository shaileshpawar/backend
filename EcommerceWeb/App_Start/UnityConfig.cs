using EcommerceWeb.Repository;
using EcommerceWeb.Services;
using System.Web.Http;
using Unity;
using Unity.Lifetime;
using Unity.WebApi;

namespace EcommerceWeb
{
    public static class UnityConfig
    {
        public static void RegisterComponents()
        {
			var container = new UnityContainer();

 

            container.RegisterType<IProductRepository, ProductRepository>(new HierarchicalLifetimeManager());
            container.RegisterType<IOrdersRepository, OrdersRepository>(new HierarchicalLifetimeManager());
            container.RegisterType<ICustomersRepository, CustomerRepository>(new HierarchicalLifetimeManager());
            container.RegisterType<ICategoriesRepository, CategoryRepository>(new HierarchicalLifetimeManager());
            container.RegisterType<ICartRepository, CartRepository>(new HierarchicalLifetimeManager());
            container.RegisterType<ILoginRepository, LoginRepository>(new HierarchicalLifetimeManager());

            GlobalConfiguration.Configuration.DependencyResolver = new UnityDependencyResolver(container);
        }
    }
}