﻿using DAL.Models;
using EcommerceWeb.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace EcommerceWeb.Controllers
{
    [RoutePrefix("api/cart")]
    public class CartController : ApiController
    {
        private ICartRepository _repository;
        public CartController(ICartRepository cartRepo)
        {
            _repository = cartRepo;
        }

        [Route("getOrders")]
        [HttpGet]
        public IEnumerable<Cart> Get()
        {
            return _repository.getCartDetails();
        }

        [Route("deleteOrder")]
        [HttpDelete]
        public void DeleteOrder([FromUri]int cartId)
        {
            _repository.deleteOrderById(cartId);
        }


        [Route("addOrder")]
        [HttpPost]
        public void Post([FromBody]Cart obj)
        {
            _repository.addToCart(obj);
        }

    }
}
