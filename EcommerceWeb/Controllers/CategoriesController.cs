﻿using DAL.Models;
using EcommerceWeb.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace EcommerceWeb.Controllers
{
    [RoutePrefix("api/categories")]
    public class CategoriesController : ApiController
    {
        private ICategoriesRepository _repository;
        public CategoriesController(ICategoriesRepository categoryRepo)
        {
            _repository = categoryRepo;
        }


      //  [Authorize(Roles = "User,Admin")]
        [Route("getCategories")]
        [HttpGet]
        public IEnumerable<object> GetAllCategories()
        {
            return _repository.getAllCategories();

        }
       // [Authorize(Roles = "Admin")]
        [Route("deleteCategory")]
        [HttpDelete]
        public void DeleteProduct([FromUri]int categoryId)
        {
            _repository.deleteCategoryById(categoryId);
        }

       // [Authorize(Roles = "Admin")]
        [Route("addCategory")]
        [HttpPost]
        public void Post([FromBody]Categories obj)
        {
            _repository.postCategory(obj);
        }
    }
}
