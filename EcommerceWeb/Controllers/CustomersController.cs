﻿using DAL.Models;
using EcommerceWeb.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace EcommerceWeb.Controllers
{
    [RoutePrefix("api/customers")]
    public class CustomersController : ApiController
    {
        private ICustomersRepository _repository;
        public CustomersController(ICustomersRepository customerRepo)
        {
            _repository = customerRepo;
        }
        [Route("allCustomers")]
        [HttpGet]
        public IEnumerable<Customers> Get()
        {
            return _repository.getAllCustomers();
        }

        [Route("addCustomer")]
        [HttpPost]
        public void Post([FromBody]Customers obj)
        {
            _repository.postCustomer(obj);

        }


        [HttpDelete]
        [Route("deleteCustomer")]
        public void DeleteCustomer([FromUri] int userId)
        {
            _repository.deleteCustomerById(userId);
        }

        [HttpPut]
        [Route("updateCustomer")]
        public void UpdateCustomer([FromUri] int userId, string email)
        {
            _repository.updateCustomerById(userId, email);
        }
    }
}
