﻿using DAL.Models;
using EcommerceWeb.Models;
using EcommerceWeb.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace EcommerceWeb.Controllers
{
    
    [RoutePrefix("api/login")]
    public class LoginController : ApiController
    {
        private ILoginRepository _repository;
        public LoginController(ILoginRepository loginRepo)
        {
            _repository = loginRepo;
        }

        [Route("userLogin")]
        [HttpPost]
        public HttpResponseMessage userLogin([FromBody]Customers customer)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    Customers customerObj = _repository.getCustomer(customer);

                    if (customerObj == null)
                    {
                        return Request.CreateResponse(HttpStatusCode.NotFound, "The user was not found.");
                    }

                    //customerObj.password = Decryption.ConvertToDecrypt(customerObj.password);

                    bool credentials = _repository.isValidCredentials(customer);

                    if (!credentials) return Request.CreateResponse(HttpStatusCode.Forbidden,
                        "The username/password combination was wrong.");

                    return Request.CreateResponse(HttpStatusCode.OK, TokenManager.GenerateToken(customerObj.firstName,
                        customerObj.RoleList.roleName));
                }

                return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Server not found");

            }
            catch (Exception e)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, e);
            }



        }
    }
}
