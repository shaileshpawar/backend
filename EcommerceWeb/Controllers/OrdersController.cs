﻿using DAL.Models;
using EcommerceWeb.Models;
using EcommerceWeb.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace EcommerceWeb.Controllers
{
    [RoutePrefix("api/orders")]
    public class OrdersController : ApiController
    {
        private IOrdersRepository _repository;
        public OrdersController(IOrdersRepository orderRepo)
        {
            _repository = orderRepo;
        }


        [Route("getOrders")]
        [HttpGet]
        public IEnumerable<Orders> Get()
        {
            return _repository.getAllOrders();
        }


        [Route("deleteOrder")]
        [HttpDelete]
        public void DeleteOrders([FromUri]int orderId)
        {
            _repository.deleteOrders(orderId);
        }

        [Route("addOrder")]
        [HttpPost]
        public void Post([FromBody]Orders obj)
        {
            _repository.postOrder(obj);

        }


        [Route("orderDetails")]
        [HttpGet]
        public IEnumerable<ProductCart> GetDetails([FromUri] int userId)
        {
            return _repository.getOrderDetailsById(userId);
        }

        [Route("placeOrder")]
        [HttpPost]
        public void PlaceOrder([FromBody] ProductCart obj)
        {
            _repository.placeOrder(obj);
        }
    }
}
