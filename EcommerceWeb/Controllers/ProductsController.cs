﻿using DAL.Models;
using EcommerceWeb.Models;
using EcommerceWeb.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace EcommerceWeb.Controllers
{
    [RoutePrefix("api/products")]
    public class ProductsController : ApiController
    {
        private IProductRepository _repository;

        public ProductsController(IProductRepository productRepo)
        {
            _repository = productRepo;
        }

       // [Authorize(Roles = "Admin")]
        [Route("getAllProducts")]

        [HttpGet]
        public IEnumerable<Object> GetAllproducts()
        {
            return _repository.getAllProducts();

        }


        [Route("postProduct")]
        [HttpPost]
        public void PostProduct([FromBody]Products obj)
        {
            _repository.postProduct(obj);
        }

        [Route("deleteProduct")]
        [HttpPost]
        public void DeleteProduct([FromBody]Products product)
        {
            _repository.deleteProductById(product);
        }

        [Route("updateProduct")]
        [HttpPut]
        public void UpdateProduct([FromBody]Products product)
        {
            _repository.updateProductById(product);
        }




    }
}
