﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace EcommerceWeb.Models
{
    public class Decryption
    {
        public static string ConvertToDecrypt(string base64EncodedPassword)
        {
            if (string.IsNullOrEmpty(base64EncodedPassword))
            {
                return "";
            }
            var base64EncodedBytes = Convert.FromBase64String(base64EncodedPassword);

            var result = Encoding.UTF8.GetString(base64EncodedBytes);
            result = result.Substring(0, result.Length - 63);

           
            return result;

        }
    }
}