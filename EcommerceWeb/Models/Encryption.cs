﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace EcommerceWeb.Models
{
    public class Encryption
    {
        private static string key = "1f581fdfdsfjhkjjh28f-80gffh95-41hjhjed-8bf8-82blkjljkf808174a58";
        
        public static string ConvertToEncrypt(string password)
        {
            if (string.IsNullOrEmpty(password))
            {
                return "";
            }
          
            var resultedString = "";

          
            resultedString =  key + password;
           

            var passwordBytes = Encoding.UTF8.GetBytes(resultedString);

            var encodedPassword = Convert.ToBase64String(passwordBytes);

            return encodedPassword;

        }
    }
}