﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EcommerceWeb.Models
{
    public class ProductCart
    {
        public string ProductName { get; set; }
        public string CustomerName { get; set; }
        public float Price { get; set; }
        public string Category { get; set; }
        public DateTime Date { get; set; }
        public int Quantity { get; set; }
        public int OrderID { get; set; }


    }
}