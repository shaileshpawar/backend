﻿using DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EcommerceWeb.Repository
{
    public interface ICartRepository
    {
        IEnumerable<Cart> getCartDetails();
        void deleteOrderById(int cartId);
        void addToCart(Cart obj);

    }
}
