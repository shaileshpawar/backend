﻿using DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EcommerceWeb.Repository
{
    public interface ICategoriesRepository
    {
        IEnumerable<object> getAllCategories();
        void deleteCategoryById(int categoryId);
        void postCategory(Categories obj);
    }
}
