﻿using DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EcommerceWeb.Repository
{
    public interface ICustomersRepository : IDisposable
    {
        IEnumerable<Customers> getAllCustomers();
        void postCustomer(Customers obj);
        void deleteCustomerById(int userId);
        void updateCustomerById(int userId, string email);

        int getCustomerName(string username);

    }
}
