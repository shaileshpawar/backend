﻿using DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EcommerceWeb.Repository
{
    public interface ILoginRepository : IDisposable
    {
        Customers getCustomer(Customers customer);
        bool isValidCredentials(Customers customer);
    }
}
