﻿using DAL.Models;
using EcommerceWeb.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EcommerceWeb.Repository
{
    public interface IOrdersRepository : IDisposable
    {

        IEnumerable<Orders> getAllOrders();
        void deleteOrders(int orderId);
        void postOrder(Orders obj);
        IEnumerable<ProductCart> getOrderDetailsById(int userId);
        void placeOrder(ProductCart obj);

    }
}
