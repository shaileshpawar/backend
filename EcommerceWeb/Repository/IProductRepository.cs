﻿using DAL.Models;
using EcommerceWeb.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EcommerceWeb.Repository
{
    public interface IProductRepository : IDisposable
    {

        IEnumerable<Object> getAllProducts();
        void postProduct(Products obj);
        void deleteProductById(Products product);
        void updateProductById(Products product);


    }
}
