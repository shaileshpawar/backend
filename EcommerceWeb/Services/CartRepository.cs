﻿using DAL.Models;
using EcommerceWeb.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EcommerceWeb.Services
{
    public class CartRepository : ICartRepository, IDisposable
    {
        EcommerceContextDB entity = new EcommerceContextDB();
        public void addToCart(Cart obj)
        {
            using (entity)
            {
                entity.Cart.Add(obj);
                entity.SaveChanges();
            }
        }

        public void deleteOrderById(int cartId)
        {
            using (entity)
            {
                var Obj = entity.Orders.Find(cartId);
                entity.Orders.Remove(Obj);
                entity.SaveChanges();
            }
        }

        protected void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (entity != null)
                {
                    entity.Dispose();
                    entity = null;
                }
            }
        }


        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public IEnumerable<Cart> getCartDetails()
        {
            using (entity)
            {
                return entity.Cart.ToList();
            }
        }
    }
}