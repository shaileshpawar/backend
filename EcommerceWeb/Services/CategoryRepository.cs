﻿using DAL.Models;
using EcommerceWeb.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EcommerceWeb.Services
{
    public class CategoryRepository : ICategoriesRepository, IDisposable
    {
        EcommerceContextDB entity = new EcommerceContextDB();
        List<Categories> tempList_GetAllCategories;
        public void deleteCategoryById(int categoryId)
        {
            using (entity)
            {
                var Obj = entity.Categories.Find(categoryId);
                entity.Categories.Remove(Obj);
                entity.SaveChanges();
            }
        }

        protected void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (entity != null)
                {
                    entity.Dispose();
                    entity = null;
                }
            }
        }


        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        public IEnumerable<object> getAllCategories()
        {
            try
            {
                tempList_GetAllCategories = (from d in entity.Categories select d).ToList();

            }
            catch (Exception e) { }
            return tempList_GetAllCategories.ToList().Select(p => new
            {
                categoryId = p.categoryId,
                categoryName = p.categoryName
            }).AsEnumerable();
        }

        public void postCategory(Categories obj)
        {
            using (entity)
            {
                entity.Categories.Add(obj);
                entity.SaveChanges();
            }
        }
    }
}