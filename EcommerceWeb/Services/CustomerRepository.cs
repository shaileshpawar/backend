﻿using DAL.Models;
using EcommerceWeb.Models;
using EcommerceWeb.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EcommerceWeb.Services
{
    public class CustomerRepository : ICustomersRepository, IDisposable
    {
        EcommerceContextDB entity = new EcommerceContextDB();
        List<Customers> tempList_GetAllCustomers;

        public void deleteCustomerById(int userId)
        {
            using (entity)
            {
                var Obj = entity.Customers.Find(userId);
                entity.Customers.Remove(Obj);
                entity.SaveChanges();
            }
        }

        protected void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (entity != null)
                {
                    entity.Dispose();
                    entity = null;
                }
            }
        }
        public IEnumerable<Customers> getAllCustomers()
        {
            using (entity)
            {
                return entity.Customers.ToList();
            }
        }

        public int getCustomerName(string username)
        {
            using (entity)
            {
                int a = (from c in entity.Customers
                         where c.firstName == username
                         select c.customerId).FirstOrDefault();
                return a;
            }
        }

        public void postCustomer(Customers obj)
        {
           // obj.password = Encryption.ConvertToEncrypt(obj.password);
            using (entity)
            {
                entity.Customers.Add(obj);
                entity.SaveChanges();
            }
        }

        public void updateCustomerById(int userId, string email)
        {
            using (entity)
            {
                var Obj = entity.Customers.Find(userId);
                Obj.email = email;
                entity.SaveChanges();
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}