﻿using DAL.Models;
using EcommerceWeb.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EcommerceWeb.Services
{
    public class LoginRepository : ILoginRepository, IDisposable
    {
        EcommerceContextDB entity = new EcommerceContextDB();
        protected void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (entity != null)
                {
                    entity.Dispose();
                    entity = null;
                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        public Customers getCustomer(Customers customer)
        {
            using (entity)
            {

                var obj = entity.Customers.Include("RoleList").Where(s => s.email.Equals(customer.email)
                && s.password.Equals(customer.password)).FirstOrDefault();

                return obj;
            }
        }

        public bool isValidCredentials(Customers customer)
        {
            using (EcommerceContextDB entity = new EcommerceContextDB())
            {
                var isvalid = (from cust in entity.Customers
                               where cust.email == customer.email &&
                                cust.password == customer.password
                               select cust).Any();

                return isvalid;
            }
        }
    }
}