﻿using DAL.Models;
using EcommerceWeb.Models;
using EcommerceWeb.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EcommerceWeb.Services
{
    public class OrdersRepository : IOrdersRepository, IDisposable
    {
        EcommerceContextDB entity = new EcommerceContextDB();
        public void deleteOrders(int orderId)
        {
            using (entity)
            {
                var obj = entity.Orders.Find(orderId);
                entity.Orders.Remove(obj);
                entity.SaveChanges();
            }
        }

        protected void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (entity != null)
                {
                    entity.Dispose();
                    entity = null;
                }
            }
        }

        public void Dispose()
        {

            Dispose(true);
            GC.SuppressFinalize(this);
        }
        public IEnumerable<Orders> getAllOrders()
        {
            using (entity)
            {
                return entity.Orders.ToList();
            }
        }

        public IEnumerable<ProductCart> getOrderDetailsById(int userId)
        {
            throw new NotImplementedException();
        }

        public void placeOrder(ProductCart obj)
        {
            throw new NotImplementedException();
        }

        public void postOrder(Orders obj)
        {
            using (entity)
            {
                entity.Orders.Add(obj);
                entity.SaveChanges();
            }
        }
    }
}