﻿using DAL.Models;
using EcommerceWeb.Models;
using EcommerceWeb.Repository;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Diagnostics;
using System.Linq;
using System.Web;

namespace EcommerceWeb.Services
{
    public class ProductRepository : IProductRepository, IDisposable
    {
        EcommerceContextDB entity = new EcommerceContextDB();
        List<Products> tempList_GetAllProducts;

        public void deleteProductById(Products product)
        {
            try
            {
                using (entity)
                {
                    var obj = entity.Products.Find(product.productId);

                    obj.productStatus = product.productStatus;
                    entity.Entry(obj).State = EntityState.Modified;
                    entity.SaveChanges();
                }

            }
            catch (DbUpdateConcurrencyException)
            {

            }


        }

        public void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (entity != null)
                {
                    entity.Dispose();
                    entity = null;
                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public IEnumerable<object> getAllProducts()
        {
            try
            {
                tempList_GetAllProducts = (from d in entity.Products select d).ToList();

            }
            catch (Exception e) {  }
            return tempList_GetAllProducts.ToList().Select(p => new
            {
                productId = p.productId,
                productName = p.productName,
                price = p.price,
                quantity = p.quantity,
                productImage = p.productImage,
                categoryId = p.categoryId
            }).AsEnumerable();
        }
           
        

        public void postProduct(Products obj)
        {
            try
            {
                using (entity)
                {
                    entity.Products.Add(obj);
                    entity.SaveChanges();
                }

            }
            catch (Exception e)
            {
                Debug.WriteLine("error=" + e);
            }
        }

        public void updateProductById(Products product)
        {
            try
            {
                using (entity)
                {
                    var temp = entity.Products.Find(product.productId);

                    temp.productName = product.productName;
                    temp.categoryId = product.categoryId;
                    temp.price = product.price;
                    temp.productStatus = product.productStatus;
                    temp.quantity = product.quantity;

                    entity.Entry(temp).State = EntityState.Modified;
                    entity.SaveChanges();
                }

            }
            catch (Exception e)
            {
                Debug.WriteLine("error=" + e);
            }
        }
    }
}